module HeaderHelper
  def current_link_class(path)
    current_page?(path) ? 'nav-link px-2 link-secondary' : 'nav-link px-2 link-body-emphasis'
  end

  def root_link_class(path)
    (current_page?(path) || current_page?('')) ? 'nav-link px-2 link-secondary' : 'nav-link px-2 link-body-emphasis'
  end
end
