ActiveAdmin.register Article do
  filter :title
  filter :created_at
  filter :author, as: :select, collection: -> { User.pluck(:email, :id) }
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
   permit_params :title, :body, :published_at, :author_id
  #
  # or
  #
  # permit_params do
  #   permitted = [:title, :body, :published_at, :author_id]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

end
