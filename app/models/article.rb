class Article < ApplicationRecord
  extend FriendlyId

  has_rich_text :body
  has_one_attached :preview_image
  belongs_to :author, class_name: 'User', foreign_key: :author_id

  friendly_id :title, use: :slugged

  def should_generate_new_friendly_id?
    slug.blank? || title_changed?
  end

  validates :title, presence: true
  validates :body, presence: true
  validates :preview_image, presence: true
  def self.ransackable_associations(auth_object = nil)
    ["author", "preview_image_attachment", "preview_image_blob", "rich_text_body"]
  end
  def self.ransackable_attributes(auth_object = nil)
    ["author_id", "body", "created_at", "id", "id_value", "published_at", "title", "updated_at"]
  end
end
