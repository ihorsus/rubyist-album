FactoryBot.define do
  factory :article do
    association :author, factory: :user

    title { Faker::Book.title }
    body { Faker::Lorem.paragraph }
    slug { Faker::Internet.slug }
    preview_image { File.open 'spec/fixtures/earth.jpg' }
  end
end
