describe Article do
  it { should belong_to(:author).class_name("User") }

  it { should validate_presence_of(:title) }
  it { should validate_presence_of(:body) }
  it { should validate_presence_of(:preview_image) }

  it { should have_one_attached(:preview_image) }

  it { should have_rich_text(:body) }

  describe ".should_generate_new_friendly_id?" do
    subject { article.should_generate_new_friendly_id? }

    context "when slug present" do
      let(:article) { create :article, slug: 'some_slug'}
      it { expect(subject).to be false }
    end

    context "when slug blank" do
      let(:article) { build :article, slug: nil, title: nil }
      it { expect(subject).to be true }
    end

  end
end
