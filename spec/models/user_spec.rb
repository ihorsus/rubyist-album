describe User, type: :model do
  let(:user) { create(:user) }

  it 'is expected to have valid factory ' do
    expect(user.valid?).to eq true
  end

  it { should have_many(:articles)}

  it { should validate_presence_of(:email) }
  it { should validate_presence_of(:password) }

end
