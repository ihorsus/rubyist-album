Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  devise_for :users, controllers: {
    omniauth_callbacks: 'users/omniauth_callbacks',
  }
  ActiveAdmin.routes(self)
  resources :articles
  #devise_for :users
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  get 'about', to: 'album#about'
  get 'news', to: 'articles#index'
  get 'cooking', to: 'articles#index'
  get 'roastering', to: 'articles#index'
  get 'other', to: 'articles#index'

  # Reveal health status on /up that returns 200 if the app boots with no exceptions, otherwise 500.
  # Can be used by load balancers and uptime monitors to verify that the app is live.
  get "up" => "rails/health#show", as: :rails_health_check

  # Defines the root path route ("/")
  root "articles#index"
end
